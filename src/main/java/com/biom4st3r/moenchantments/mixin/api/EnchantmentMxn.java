package com.biom4st3r.moenchantments.mixin.api;

import java.util.List;

import com.biom4st3r.moenchantments.api.ExtendedEnchantment;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.AnvilScreenHandler;

@Mixin(Enchantment.class)
public class EnchantmentMxn implements ExtendedEnchantment {

    @Override
    public boolean isExtended() {
        return false;
    }

    @Override
    public void applicationLogic(int power, ItemStack stack, boolean treasureAllowed,List<EnchantmentLevelEntry> entries) {

    }

    @Shadow public native boolean isAcceptableItem(ItemStack stack);

    @Override
    public boolean isAcceptable(ItemStack is) {
        return isAcceptableItem(is);
    }

    @Override
    public boolean isAcceptibleInAnvil(ItemStack is, AnvilScreenHandler anvil) {
        return isAcceptableItem(is);
    }

    @Override
    public boolean isAcceptibleOnBook() {
        return true;
    }
    
}
