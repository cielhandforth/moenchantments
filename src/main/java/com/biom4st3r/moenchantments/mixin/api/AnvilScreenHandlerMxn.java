package com.biom4st3r.moenchantments.mixin.api;

import com.biom4st3r.moenchantments.api.ExtendedEnchantment;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.AnvilScreenHandler;

@Mixin(AnvilScreenHandler.class)
public abstract class AnvilScreenHandlerMxn {
    
    @Redirect(
        method = "updateResult",
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/enchantment/Enchantment.isAcceptableItem(Lnet/minecraft/item/ItemStack;)Z",
            ordinal = 0))
    private boolean replaceIsAcceptibleItem(Enchantment enchant,ItemStack is)
    {
        ExtendedEnchantment ex = ExtendedEnchantment.cast(enchant);
        return ex.isExtended() ? ex.isAcceptibleInAnvil(is, (AnvilScreenHandler)(Object)this) : enchant.isAcceptableItem(is);
    }
}
