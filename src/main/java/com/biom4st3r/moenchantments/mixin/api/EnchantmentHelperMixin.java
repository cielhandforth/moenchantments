
package com.biom4st3r.moenchantments.mixin.api;

import java.util.Iterator;
import java.util.List;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.api.EnchantmentSkeleton;
import com.biom4st3r.moenchantments.api.ExtendedEnchantment;
import com.google.common.collect.Lists;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.registry.Registry;

@Mixin(EnchantmentHelper.class)
public abstract class EnchantmentHelperMixin {
/*
slice = @Slice(
   from = @At(value="INVOKE",target="java/util/Iterator.next()Ljava/lang/Object;"),
   to = @At(value="INVOKE",target="net/minecraft/enchantment/Enchantment.isTreasure()Z")
   ),
*/

   @Inject(
      at = @At(
         value="INVOKE",
         target="net/minecraft/enchantment/Enchantment.isTreasure()Z",
         ordinal = 0,
         shift = Shift.BEFORE
         ),
      method = "getPossibleEntries",
      cancellable = false,
      locals = LocalCapture.CAPTURE_FAILHARD)
   private static void isAcceptibleForMoEnchantment(int power, ItemStack stack, 
      boolean treasureAllowed, CallbackInfoReturnable<List<EnchantmentLevelEntry>> cir, 
      List<EnchantmentLevelEntry> list, Item item, boolean bl, Iterator<Enchantment> enchantmentIterator, 
      Enchantment enchantment)
   {
      while(enchantment instanceof EnchantmentSkeleton && enchantmentIterator.hasNext())
      {
         EnchantmentSkeleton mEnch = (EnchantmentSkeleton) enchantment;
         if(!mEnch.isAcceptableItem(stack) || (enchantment.isTreasure() && !treasureAllowed) || !enchantment.isAvailableForRandomSelection())
         {
            enchantment = enchantmentIterator.next();
         }
         else
         {
            for (int i = enchantment.getMaxLevel(); i > enchantment.getMinLevel() - 1; --i) {
               if (power >= enchantment.getMinPower(i) && power <= enchantment.getMaxPower(i)) {
                  list.add(new EnchantmentLevelEntry(enchantment, i));
                  break;
               }
            }
            enchantmentIterator.next();
         }
      }
   }

   @Inject(
      at = @At(
         value="INVOKE",
         target="java/util/List.add(Ljava/lang/Object;)Z",
         ordinal = -1,
         shift = Shift.BEFORE),
      method = "getPossibleEntries",
      cancellable = false,
      locals = LocalCapture.CAPTURE_FAILHARD)
   private static void invertEndermanCurse(int power, ItemStack stack, 
      boolean treasureAllowed, CallbackInfoReturnable<List<EnchantmentLevelEntry>> cir, 
      List<EnchantmentLevelEntry> list, Item item, boolean bl, Iterator<Enchantment> var6, 
      Enchantment enchantment, int i)
   {
      if(enchantment == EnchantmentRegistry.ENDERPROTECTION)
      {
         i = (i == 3 ? 1 : i == 1 ? 3 : i);
      }
   }

   // @Overwrite
   // public static List<EnchantmentLevelEntry> getPossibleEntries(int power, ItemStack stack, boolean treasureAllowed) 
   // {
   //    Iterator<Enchantment> iter = Registry.ENCHANTMENT.iterator();
   //    List<EnchantmentLevelEntry> list = Lists.newArrayList();
   //    while (iter.hasNext())
   //    {
   //       Enchantment enchant = iter.next();
   //       ExtendedEnchantment extended = ExtendedEnchantment.cast(enchant);
   //       boolean success = true;
   //       if(extended.isExtended() && extended.providesApplicationLogic()) 
   //       {
   //          extended.applicationLogic(power, stack, treasureAllowed, list);
   //          continue;
   //       }
   //       boolean isBook = stack.getItem() == Items.BOOK;
   //       success &= 
   //          extended.isExtended() 
   //          ? extended.isAcceptable(stack) || (isBook & extended.isAcceptable(stack))
   //          : enchant.type.isAcceptableItem(stack.getItem()) || isBook;
   //       success &= enchant.isTreasure() && treasureAllowed;
   //       success &= enchant.isAvailableForRandomSelection();
   //       if(!success) continue;
   //       for(int lvl = enchant.getMaxLevel(); lvl > enchant.getMinLevel() -1; --lvl)
   //       {
   //          if(power >= enchant.getMinPower(lvl) && power <= enchant.getMaxPower(lvl))
   //          {
   //             list.add(new EnchantmentLevelEntry(enchant, lvl));
   //          }
   //       }
   //    }
   //    return list;
   // }
}
