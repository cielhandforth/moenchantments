package com.biom4st3r.moenchantments.mixin;

import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Slice;

import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.network.packet.s2c.play.EntitySpawnS2CPacket;
import net.minecraft.util.registry.Registry;

@Mixin(ClientPlayNetworkHandler.class)
public abstract class NonLivingEntityClientSpawnFix {

    @Shadow private ClientWorld world;

    @Inject(
        at = @At(
            value = "JUMP",
            opcode = Opcodes.IFNULL,
            shift = Shift.BEFORE
        ),
        method = "onEntitySpawn",
        slice = @Slice(
            from = @At(
                value = "INVOKE",
                target = "net/minecraft/entity/LightningEntity.<init>(Lnet/minecraft/entity/EntityType;Lnet/minecraft/world/World;)V"
            ),
            to = @At(
                value = "INVOKE",
                target = "net/minecraft/network/packet/s2c/play/EntitySpawnS2CPacket.getId()I"
            )
        ),
        locals = LocalCapture.CAPTURE_FAILHARD
    )
    private void attemptSpawnNonVanillaEntity(EntitySpawnS2CPacket packet,CallbackInfo ci,double x, double y, double z, Entity entity, EntityType<? extends Entity> entityType)
    {
        if(entity == null && entityType != Registry.ENTITY_TYPE.get(-1))
        {
            entity = entityType.create(world);
            if(entity != null)
            {
                entity.setVelocity(packet.getVelocityX(), packet.getVelocityY(), packet.getVelocityZ());
                if(entity instanceof ProjectileEntity) ((ProjectileEntity)entity).setOwner(world.getEntityById(packet.getEntityData()));
            }
        }
    }

    
}