package com.biom4st3r.moenchantments.mixin.soulbound;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.biom4st3r.moenchantments.logic.SoulboundBindings;
import com.biom4st3r.moenchantments.logic.SoulboundBindings.InventoryTarget;
import com.google.common.collect.Lists;

import org.apache.commons.lang3.tuple.Pair;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;

/**
 * Makes a map of combinedInventory matching a backup list to the source list in sourceToBackupMap<p>
 */
@Mixin(PlayerInventory.class)
public abstract class PlayerInventoryMxn {
    @Shadow
    @Final
    private List<DefaultedList<ItemStack>> combinedInventory;
    @Shadow
    @Final
    public DefaultedList<ItemStack> main;
    @Shadow
    @Final
    public DefaultedList<ItemStack> armor;
    @Shadow
    @Final
    public DefaultedList<ItemStack> offHand;

    @Shadow
    @Final
    public PlayerEntity player;

    private List<Pair<List<ItemStack>,List<ItemStack>>> sourceToBackup = Lists.newArrayList();

    // private Map<List<ItemStack>, List<ItemStack>> sourceToBackupMap = new Object2ObjectOpenHashMap<>();

    @Inject(at = @At("HEAD"), method = "dropAll")
    public void biom4st3r_initBackupLists(CallbackInfo ci) {
        sourceToBackup.clear();
        for (List<ItemStack> list : combinedInventory) {
            List<ItemStack> backup = new ArrayList<>(list.size());
            for (int i = 0; i < list.size(); i++) {
                backup.add(null);
            }
            sourceToBackup.add(Pair.of(list, backup));
        }

        // mainBackup = new ArrayList<>(main.size());
        // ModInit.logger.debug("main: %s", main.size());
        // armorBackup = new ArrayList<>(armor.size());
        // ModInit.logger.debug("armor: %s", armor.size());
        // offHandBackup = new ArrayList<>(offHand.size());
        // ModInit.logger.debug("offhan: %s", offHand.size());
        // for(int i = 0; i< main.size();i++)
        // {
        // if(i < offHand.size()) offHand.add(null);
        // if(i < armor.size()) armor.add(null);
        // main.add(null);
        // }
    }

    @Inject(at = @At(value = "INVOKE_ASSIGN", target = "java/util/List.get(I)Ljava/lang/Object;", shift = Shift.BY, by = 2), method = "dropAll", cancellable = false, locals = LocalCapture.CAPTURE_FAILHARD)
    public void biom4st3r_providerContext(CallbackInfo ci, Iterator<List<ItemStack>> var1, List<ItemStack> list,
            int index) {
        this.biom4st3r_currentList = list;
        this.biom4st3r_index = index;
        this.biom4st3r_currentTarget = list == main ? InventoryTarget.MAIN
        : list == armor ? InventoryTarget.ARMOR
                : list == offHand ? InventoryTarget.OFFHAND : InventoryTarget.UNKNOWN;
    }
    
    private  int biom4st3r_index;
    private List<ItemStack> biom4st3r_currentList;
    private SoulboundBindings.InventoryTarget biom4st3r_currentTarget;

    /**
     * this is dumb, but when i tried to use a Map for this i kept getting null...
     * @param list
     * @return
     */
    private List<ItemStack> getBackupFromSource(List<ItemStack> list)
    {
        for(Pair<List<ItemStack>, List<ItemStack>> i : sourceToBackup)
        {
            if(list == i.getLeft()) return i.getRight();
        }
        return null;
    }

    /**
     * This will short circuit the drop method by setting the item to empty
     * 
     * @param is
     * @return
     */
    @ModifyVariable(method = "dropAll", at = @At(value = "INVOKE_ASSIGN", target = "java/util/List.get(I)Ljava/lang/Object;", shift = Shift.BY, by = 2), print = false)
    public ItemStack biom4st3r_checkInventoriesForSoulbound(ItemStack is) {
        if(is.isEmpty()) return is;
        List<ItemStack> backupList = getBackupFromSource(biom4st3r_currentList);
        return SoulboundBindings.ATTEMPT_DROP_BEHAVIOR.shouldDropItemOnDeath(biom4st3r_currentTarget, is, biom4st3r_currentList,
                backupList, biom4st3r_index, (PlayerInventory) (Object) this) ? is : ItemStack.EMPTY;
    }


    @Inject(at = @At("RETURN"), method = "dropAll")
    public void restoreSoulBoundItems(CallbackInfo ci) {
        for (Pair<List<ItemStack>, List<ItemStack>> pair : sourceToBackup)
        {
            SoulboundBindings.InventoryTarget target = pair.getLeft() == main ? InventoryTarget.MAIN
            : pair.getLeft() == armor ? InventoryTarget.ARMOR
                    : pair.getLeft() == offHand ? InventoryTarget.OFFHAND : InventoryTarget.UNKNOWN;

            for(int i = 0; i < pair.getRight().size();i++)
            {
                ItemStack stack = pair.getRight().get(i);
                if(stack == null) continue;
                SoulboundBindings.ATTEMPT_RESTORE.restore(target, stack, pair.getLeft(), i, (PlayerInventory)(Object)this);

            }
        }
    }

}