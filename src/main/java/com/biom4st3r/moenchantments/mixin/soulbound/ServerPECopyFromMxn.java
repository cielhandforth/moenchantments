package com.biom4st3r.moenchantments.mixin.soulbound;

import com.biom4st3r.moenchantments.logic.SoulboundBindings;
import com.mojang.authlib.GameProfile;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

@Mixin(ServerPlayerEntity.class)
public abstract class ServerPECopyFromMxn extends PlayerEntity {

    public ServerPECopyFromMxn(World world, BlockPos blockPos, float f, GameProfile gameProfile) {
        super(world, blockPos, f, gameProfile);
    }

    @Inject(at = @At(value = "FIELD", target = "Lnet/minecraft/server/network/ServerPlayerEntity;enchantmentTableSeed:I"), method = "copyFrom")
    public void copySoulboundItems(ServerPlayerEntity spe, boolean alive, CallbackInfo ci)
    {
        SoulboundBindings.COPY_SOULBOUND_ITEMS_ON_RESPAWN.copy(spe, (ServerPlayerEntity)(PlayerEntity)this, alive);
    }
}