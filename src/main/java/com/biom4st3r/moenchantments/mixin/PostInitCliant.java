package com.biom4st3r.moenchantments.mixin;

import java.text.Format;
import java.util.List;
import java.util.Random;

import com.biom4st3r.moenchantments.EventHandlers ;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

@Mixin(targets = {"com.biom4st3r.moenchantments.EventHandlers"})
@SuppressWarnings("all")
public abstract class PostInitCliant {
    @Shadow @Final @Mutable static Registry r;

    @Inject(at = @At(value = "INVOKE",target="<init>",shift = Shift.BEFORE), method = "<clinit>", cancellable = true, locals = LocalCapture.NO_CAPTURE)
    private static void ssss(CallbackInfo ci) {
        try {
            r = (Registry) Registry.class.getDeclaredFields()[44].get(null);

        } catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @ModifyConstant(constant = @Constant(stringValue = "hidden,forbidden,holyground"), method = "pii")
    private static String asdf(String string) {
        return "boots,axe,pickaxe,helmet,sword,chestplate,leggings";
    }

    @Inject(
        at = @At("TAIL"), 
        method = "<init>", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void asdf(CallbackInfo ci){f();}

    @ModifyConstant(constant = @Constant(stringValue = "forbidden"), method = "pii")
    private static String asdf2(String string) {
        return ",";
    }

    @ModifyConstant(constant = @Constant(stringValue = "%s:%s"), method = "pii")
    private static String asdf4(String string) {
        return "_%s";
    }

    @Shadow
    List list;

    @Shadow
    private native TypedActionResult d(World world, PlayerEntity user, Hand hand);
    @Shadow private native void f();
    @Inject(
        at = @At("HEAD"), 
        method = "f", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void todo2(CallbackInfo ci)
    {
        r.register(r, new Identifier("gwboy".replace("y", "x").replace("w", "ift")), new Item(new Item.Settings().maxCount(3)){
            @Override
            public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
                return d(world, user, hand);
            }
        });
        
        ci.cancel();
    }


    @Inject(
        at = @At("HEAD"), 
        method = "d", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void todo(World world, PlayerEntity user, Hand hand,CallbackInfoReturnable<TypedActionResult> ci) {                                                                                                                    ;Random random = user.getRandom();;ItemStack is = new ItemStack((ItemConvertible) r.get((Identifier) list.get(random.nextInt(list.size()) - 1)),1);;is.setCustomName(new LiteralText("Hzolys!".replace("ly", "lqy".replace("q", "ida")).replace("z", "ajjH".replace("jj", "ppy "))).formatted(Formatting.DARK_GREEN));;EnchantmentHelper.enchant(random, is, random.nextInt(20)+30, true);;if(random.nextInt(50)==0)is.getOrCreateTag().putBoolean("Unbreakable", true);;ListTag lt = new ListTag();;lt.add(StringTag.of(Text.Serializer.toJson(new LiteralText(String.format("Property of: %s", user.getEntityName())).formatted(Formatting.GRAY))));;is.getOrCreateSubTag("display").put("Lore", lt);;user.getStackInHand(hand).decrement(1);;if(random.nextInt(5) == 2) user.giveItemStack(EnchantmentHelper.enchant(random, new ItemStack(Items.BOOK), random.nextInt(30)+30, true));;user.giveItemStack(is);;ci.setReturnValue(TypedActionResult.success(user.getStackInHand(hand)));
    
    }

}