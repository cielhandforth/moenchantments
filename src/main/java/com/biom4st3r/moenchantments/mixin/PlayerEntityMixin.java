package com.biom4st3r.moenchantments.mixin;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.api.EnchantmentSkeleton;
import com.biom4st3r.moenchantments.interfaces.PlayerExperienceStore;
import com.biom4st3r.moenchantments.interfaces.PotionEffectRetainer;
import com.biom4st3r.moenchantments.interfaces.PotionRetentionTarget;
import com.biom4st3r.moenchantments.logic.EnderProtectionLogic;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.World;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixin extends LivingEntity implements PlayerExperienceStore {
    public PlayerEntityMixin(EntityType<? extends LivingEntity> entityType_1, World world_1) {
        super(entityType_1, world_1);
    }

    @Unique public boolean biom4st3r_received = false;
    @Unique public float biom4st3r_experienceStorage = 0;

    @Inject(
        at = @At("TAIL"), 
        method = "writeCustomDataToTag", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void biom4st3r_saveTo(CompoundTag ct, CallbackInfo ci)
    {
        ct.putBoolean("fdo3oj5jv0s02", biom4st3r_received);
    }

    @Inject(
        at = @At("TAIL"), 
        method = "readCustomDataFromTag", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void biom4st3r_read(CompoundTag ct, CallbackInfo ci)
    {
        biom4st3r_received = ct.getBoolean("fdo3oj5jv0s02");
    }

    @Override
    public boolean setReceive() {
        if(!biom4st3r_received)
        {
            biom4st3r_received = true;
            return biom4st3r_received;
        } 
        return false;
    }

    @Override
    public int biom4st3r_getAndRemoveStoredExp() {
        if(biom4st3r_experienceStorage >= 1f)
        {
            int experienceStorage_int = (int)biom4st3r_experienceStorage;
            biom4st3r_experienceStorage = biom4st3r_experienceStorage-experienceStorage_int;
            ModInit.logger.debug("Claimed %s/%s exp", experienceStorage_int, biom4st3r_experienceStorage);
            return experienceStorage_int;
        }
        return 0; 
    }

    @Inject(at = @At("HEAD"), method = "applyDamage",cancellable = true)
    public void enderProtectionImpl(DamageSource damageSource, float damage, CallbackInfo ci) {
        LivingEntity defender = (LivingEntity) (Object) this;
        int EnderProtectionLevel = EnchantmentHelper.getEquipmentLevel(EnchantmentRegistry.ENDERPROTECTION, defender);
        if (EnderProtectionLevel > 0) {
            if (EnderProtectionLogic.doLogic(damageSource, EnderProtectionLevel, defender)) 
            {
                // ci.setReturnValue(Boolean.FALSE);
                ci.cancel();
            }
        }
    }
    
    @Override
    public void addExp(float i) {
        ModInit.logger.debug("storing %s exp", i);
        this.biom4st3r_experienceStorage += i;
    }

    @Inject(at = @At("HEAD"),method = "attack")
    public void usePotionEffectFromImbued(Entity defender,CallbackInfo ci)
    {
        PotionEffectRetainer stack = ((PotionEffectRetainer) (Object) this.getMainHandStack());
        if(EnchantmentRegistry.POTIONRETENTION.isAcceptableItem(this.getMainHandStack()) && EnchantmentSkeleton.hasEnchant(EnchantmentRegistry.POTIONRETENTION, this.getMainHandStack()) && stack.getCharges() > 0)
        {
            StatusEffectInstance effect = stack.useEffect();
            if(defender instanceof LivingEntity)
            {
                ((PotionRetentionTarget)defender).applyRetainedPotionEffect(effect);
            }
        }
    }
}