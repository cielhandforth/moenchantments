package com.biom4st3r.moenchantments.mixin;

import com.biom4st3r.moenchantments.ModInit;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.MinecraftClient;

@Mixin(MinecraftClient.class)
public abstract class PostInitClient {
    @Inject(at = @At("RETURN"), method = "<init>")
    private void postInit(CallbackInfo ci) {
        ModInit.logger.log("...and my turn again");
        ModInit.whitelistToBlock();
        ModInit.blacklistAutosmelt();
        // StringBuilder builder = new StringBuilder();
        // Registry.ENCHANTMENT.forEach((enchantment) -> {

        //     builder.append(Registry.ENCHANTMENT.getId(enchantment).toString());
        //     builder.append("\n");
        //     for (int i = 1; i <= enchantment.getMaxLevel(); i++) {
        //         builder.append(i + ": ");
        //         builder.append(enchantment.getMinPower(i)).append(" - ").append(enchantment.getMaxPower(i));
        //         builder.append("\n");
        //     }
        //     builder.append("\n");
        // });
        // File file = new File(FabricLoader.getInstance().getConfigDirectory().getPath(), "powers.txt");
        // FileWriter fw;
        // try {
        //     fw = new FileWriter(file);
        //     fw.write(builder.toString());
        //     fw.close();
        // } catch (IOException e) {
        //     e.printStackTrace();
        // }
        // throw new IllegalStateException();
    } 

}