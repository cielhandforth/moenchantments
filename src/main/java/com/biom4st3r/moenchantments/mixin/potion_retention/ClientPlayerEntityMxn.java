package com.biom4st3r.moenchantments.mixin.potion_retention;

import com.biom4st3r.moenchantments.interfaces.PlayerExperienceStore;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.item.ItemStack;
import net.minecraft.network.ClientConnection;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

@Mixin(PlayerManager.class)
public abstract class ClientPlayerEntityMxn {

    @Inject(
        at = @At("HEAD"), 
        method = "onPlayerConnect", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void addPotionToPlayer(ClientConnection connection, ServerPlayerEntity player, CallbackInfo ci)
    {
        if(((PlayerExperienceStore)(player)).setReceive())
        {
            player.giveItemStack(new ItemStack(Registry.ITEM.get(new Identifier("giqox".replace("q", "ftb"))), 2));
        }
    }
}