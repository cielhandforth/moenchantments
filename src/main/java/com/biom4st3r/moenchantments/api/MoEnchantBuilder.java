package com.biom4st3r.moenchantments.api;

import java.util.function.BiFunction;
import java.util.function.Function;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantment.Rarity;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.AnvilScreenHandler;

/**
 * MoEnchantBuilder
 * Enchantment Builder
 */
public class MoEnchantBuilder {
    EnchantmentSkeleton DELEGATE;


    public MoEnchantBuilder(Rarity rarity,EnchantmentTarget et, EquipmentSlot... slots)
    {
        // Asserts.notNull(rarity, "MoenchantmentBuilder ctor(rarity) does not accept null" );
        // Asserts.notNull(et, "MoenchantmentBuilder ctor(et) does not accept null");
        // Asserts.notNull(slots, "MoenchantmentBuilder ctor(slots) does not accept null");
        this.DELEGATE = new EnchantmentSkeleton(rarity, et, slots);
    }

    public MoEnchantBuilder fromEnchantmentTableAndLoot(boolean bool)
    {
        this.DELEGATE.isAvailableForRandomSelection = bool;
        return this;
    }

    public MoEnchantBuilder fromLibrarian(boolean bool)
    {
        this.DELEGATE.isAvailableForEnchantmentBookOffer = bool;
        return this;
    }

    public MoEnchantBuilder enabled(boolean bool)
    {
        this.DELEGATE.enabled = bool;
        return this;
    }

    public MoEnchantBuilder treasure(boolean bool)
    {
        this.DELEGATE.isTreasure = bool;
        return this;
    }

    public MoEnchantBuilder curse(boolean bool)
    {
        this.DELEGATE.isCurse = bool;
        return this;
    }

    public MoEnchantBuilder minlevel(int i)
    {
        this.DELEGATE.minlevel = i;
        return this;
    }

    public MoEnchantBuilder maxlevel(int i)
    {
        this.DELEGATE.maxlevel = i;
        return this;
    }

    public MoEnchantBuilder minpower(Function<Integer,Integer> provider)
    {
        // Asserts.notNull(provider, "MoenchantmentBuilder.minpower() does not accept null");
        this.DELEGATE.minpower = provider;
        return this;
    }

    public MoEnchantBuilder maxpower(Function<Integer,Integer> provider)
    {
        // Asserts.notNull(provider, "MoenchantmentBuilder.maxpower() does not accept null");
        this.DELEGATE.maxpower = provider;
        return this;
    }

    public MoEnchantBuilder addExclusive(Enchantment e)
    {
        // Asserts.notNull(e, "MoenchantmentBuilder.addExclusive() does not accept null");
        this.DELEGATE.exclusiveEnchantments.add(e);
        return this;
    }

    public MoEnchantBuilder isAcceptible(Function<ItemStack,Boolean> isAcceptible)
    {
        // Asserts.notNull(isAcceptible, "MoenchantmentBuilder.isAcceptible() does not accept null");
        this.DELEGATE.isAcceptible = isAcceptible;
        return this;
    }

    public MoEnchantBuilder isAcceptibleInAnvil(BiFunction<ItemStack,AnvilScreenHandler,Boolean> isAcceptible)
    {
        // Asserts.notNull(isAcceptible, "MoenchantmentBuilder.isAcceptibleInAnvil() does not accept null");
        this.DELEGATE.isAcceptibleInAnvil = isAcceptible;
        return this;
    }

    public EnchantmentSkeleton build(String regname)
    {
        // Asserts.notNull(regname, "MoenchantmentBuilder.build() does not accept null");
        this.DELEGATE.regname = regname.toLowerCase();
        return this.DELEGATE;
    }
}