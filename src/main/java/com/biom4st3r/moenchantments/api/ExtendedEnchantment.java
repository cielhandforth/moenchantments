package com.biom4st3r.moenchantments.api;

import java.util.List;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.AnvilScreenHandler;

public interface ExtendedEnchantment {
    
    boolean isExtended();

    /**
     * When enchantment {@code ExtendedEnchantment#isExtended()} this is <p>
     * this is used to replace {@code EnchantmentTarget#isAcceptableItem(net.minecraft.item.Item)}
     * in {@code EnchantmentHelper#getPossibleEntries(int, ItemStack, boolean)}
     * @param is
     * @return
     */
    boolean isAcceptable(ItemStack is);

    /**
     * When enchantment {@code ExtendedEnchantment#isExtended()} this is <p>
     * used to replace {@code Enchantment#isAcceptableItem(ItemStack)}
     * in {@code AnvilScreenHandler#updateResult()}
     * @param is
     * @param anvil
     * @return
     */
    boolean isAcceptibleInAnvil(ItemStack is, AnvilScreenHandler anvil);

    /**
     * When enchantment {@code ExtendedEnchantment#isExtended()} this is <p>
     * this is checked in {@code EnchantmentHelper#getPossibleEntries(int, ItemStack, boolean)}<p>
     * instead of stack.getItem() == Items.BOOK.
     * @return
     */
    boolean isAcceptibleOnBook();

    /**
     * Helper Method
     * @return
     */
    default Enchantment asEnchantment()
    {
        return ((Enchantment)this);
    }

    /**
     * Helper Method
     * @param e
     * @return
     */
    static ExtendedEnchantment cast(Enchantment e)
    {
        return (ExtendedEnchantment)e;
    }

    default boolean providesApplicationLogic()
    {
        return false;
    }
    
    /**
     * if {@code ExtendedEnchantment#providesApplicationLogic()} <p>
     * this will fully replace logic in {@code getPossibleEnchantments}<p>
     * for your enchantment.
     * @param power
     * @param stack
     * @param treasureAllowed
     * @param entries
     */
    void applicationLogic(int power, ItemStack stack, boolean treasureAllowed,List<EnchantmentLevelEntry> entries);

}
