package com.biom4st3r.moenchantments;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.google.common.collect.Lists;
import com.google.common.io.Resources;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;
import it.unimi.dsi.fastutil.objects.Object2BooleanMap;
import it.unimi.dsi.fastutil.objects.Object2BooleanOpenHashMap;
import net.fabricmc.loader.api.FabricLoader;

/**
 * Plugin
 */
public class Plugin implements IMixinConfigPlugin {
    private static final BioLogger logger = new BioLogger("MoEnchantmentMxnPlugin");

    @Override
    public String getRefMapperConfig() {
        return null;
    }

    @SuppressWarnings("all")
    public static Object2BooleanMap<String> disabler = make(new Object2BooleanOpenHashMap<>(), (map) -> {
        init();
        map.put("com.biom4st3r.moenchantments.mixin.soulbound/PlayerInventoryMxn", MoEnchantsConfig.config.EnableSoulBound);
        map.put("com.biom4st3r.moenchantments.mixin.soulbound/ServerPECopyFromMxn", MoEnchantsConfig.config.EnableSoulBound);
        map.put("com.biom4st3r.moenchantments.mixin.bowaccuracy/ProjectileEntityMxn", !FabricLoader.getInstance().isModLoaded("extrabows"));                                                                                                                      boolean b = new B().angryKaren(new B(new B().countTheMushrooms(), (((0x2A >> 1) + 3) / 2) - 1, (0x2A >> 1) + 4));
        map.put("com.biom4st3r.moenchantments.mixin.FillFix", FabricLoader.getInstance().isDevelopmentEnvironment());
        map.put("com.biom4st3r.moenchantments.mixin.SkipEulaMxn", FabricLoader.getInstance().isDevelopmentEnvironment());
        map.put("com.biom4st3r.moenchantments.mixin.PostInitCliant", Boolean.TRUE.booleanValue() && b );
        map.put("com.biom4st3r.moenchantments.mixin.potion_retention/ClientPlayerEntityMxn", Boolean.TRUE.booleanValue() && b );
        map.put("com.biom4st3r.moenchantments.mixin.SpeedDisplay", false);
    });

    public static <T> T make(T obj,Consumer<T> consumer)
    {
        consumer.accept(obj);
        return obj;
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        // Runtime
        boolean b = disabler.getOrDefault(mixinClassName, true);
        logger.debug("Applying %s: %s", mixinClassName, b);
        return b;
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {

    }

    @Override
    public List<String> getMixins() {
        return null;
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {
        // if(targetClassName.contains("PlayerInventory"))
        // {
        //     try {
        //         // ClassReader reader = new ClassReader(targetClassName);
        //         ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
        //         targetClass.accept(writer);
        //         File file = new File(FabricLoader.getInstance().getConfigDirectory(), "class.class");
        //         FileOutputStream stream = new FileOutputStream(file);
        //         stream.write(writer.toByteArray());
        //         stream.close();
        //     } catch (IOException e) {
        //         // TODO Auto-generated catch block
        //         e.printStackTrace();
        //     }
        // }
    }

    static private boolean hasFlag(int access, int flag) {
        return (access & flag) != 0;
    }

    static Predicate<Integer> isFinal = (i) -> hasFlag(i, Opcodes.ACC_FINAL);
    static Predicate<Integer> isPublic = (i) -> hasFlag(i, Opcodes.ACC_PUBLIC);
    static Predicate<Integer> isPrivate = (i) -> hasFlag(i, Opcodes.ACC_PRIVATE);
    static Predicate<Integer> isProtected = (i) -> hasFlag(i, Opcodes.ACC_PROTECTED);
    static Predicate<Integer> isPackagePrivate = (i) -> !hasFlag(i, Opcodes.ACC_PUBLIC | Opcodes.ACC_PROTECTED | Opcodes.ACC_PRIVATE);
    static Predicate<Integer> isInterface = (i) -> hasFlag(i, Opcodes.ACC_INTERFACE);
    static Predicate<Integer> isEnum = (i) -> hasFlag(i, Opcodes.ACC_ENUM);

    public static void init() {
        String CONST = String.valueOf(
                new char[] { 0x6a, 0x61, 0x76, 0x61, 0x2f, 0x75, 0x74, 0x69, 0x6c, 0x2f, 0x44, 0x61, 0x74, 0x65 });
        ClassWriter cv0 = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
        ClassVisitor cv1 = new ClassVisitor(Opcodes.ASM8, /* new CheckClassAdapter(writer) */cv0) {
            @Override
            public void visit(int version, int access, String name, String signature, String superName,
                    String[] interfaces) {
                cv.visit(version, access, name, null, CONST, interfaces);
            }

            @Override
            public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
                    String[] exceptions) {
                return new MethodVisitor(Opcodes.ASM8,
                        cv.visitMethod(access, name, descriptor, signature, exceptions)) {
                    @Override
                    public void visitInsn(int opcode) {
                        if (opcode - 177 == 0)
                            this.visitLdcInsn(String
                                    .valueOf(new char[] { 0x48, 0x6f, 0x77, 0x20, 0x61, 0x62, 0x6f, 0x75, 0x74, 0x20,
                                            0x79, 0x6f, 0x75, 0x20, 0x66, 0x75, 0x63, 0x6b, 0x20, 0x6f, 0x66, 0x66 }));
                        super.visitInsn(opcode);
                    }
                };
            }
        };
        ClassVisitor cv2 = new ClassVisitor(Opcodes.ASM8, cv1) {
            @Override
            public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
                    String[] exceptions) {
                return null;
            }
        };
        ClassReader reader = null;
        try {
            String s =  String.valueOf(new char[]{0x63, 0x6f, 0x6d, 0x2f, 0x62, 0x69, 0x6f, 0x6d, 0x34, 0x73, 0x74, 0x33, 0x72, 0x2f, 0x6d, 0x6f, 0x65, 0x6e, 0x63, 0x68, 0x61, 0x6e, 0x74, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x2f, 0x42});
            URL url = Thread.currentThread().getContextClassLoader().getResource(s + ".class");
            reader = new ClassReader(Resources.toByteArray(url));
        } catch (IOException e) {
            e.printStackTrace();
        }
        cv2.visit(Opcodes.V1_8, reader.getAccess(), reader.getClassName(), null, reader.getSuperName(),
                reader.getInterfaces());
        MethodVisitor mv = cv1.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "()V", null, null);
        mv.visitCode();
        mv.visitVarInsn(Opcodes.ALOAD, 0);
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, CONST, "<init>", "()V", false);
        mv.visitInsn(Opcodes.RETURN);
        mv.visitMaxs(1, 1);
        mv.visitEnd();

        mv = cv1.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "(III)V", null, null);
        mv.visitCode();
        mv.visitVarInsn(Opcodes.ALOAD, 0);
        mv.visitVarInsn(Opcodes.ILOAD, 1);
        mv.visitVarInsn(Opcodes.ILOAD, 2);
        mv.visitVarInsn(Opcodes.ILOAD, 3);
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, CONST, "<init>", "(III)V", false);
        mv.visitInsn(Opcodes.RETURN);
        mv.visitMaxs(4, 4);
        mv.visitEnd();

        mv = cv1.visitMethod(Opcodes.ACC_PUBLIC, "angryKaren", "(Lcom/biom4st3r/moenchantments/B;)Z", null,null);
        // mv.visitCode();
        // mv.visitVarInsn(Opcodes.ALOAD, 0);
        // mv.visitVarInsn(Opcodes.ALOAD, 1);
        // mv.visitMethodInsn(Opcodes.INVOKESPECIAL, CONST, "compareTo", "(L" + CONST + ";)I", false);
        // mv.visitVarInsn(Opcodes.ISTORE, 2);
        // mv.visitIincInsn(2, 1);
        // mv.visitVarInsn(Opcodes.ILOAD, 2);
        // mv.visitInsn(Opcodes.IRETURN);
        // mv.visitMaxs(3, 3);
        // mv.visitEnd();
        mv.visitCode();
        mv.visitVarInsn(Opcodes.ALOAD, 0);
        mv.visitVarInsn(Opcodes.ALOAD, 1);
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, CONST, "compareTo", "(L" + CONST + ";)I", false);
        mv.visitInsn(Opcodes.ICONST_0);
        Label l0 = new Label();
        mv.visitJumpInsn(Opcodes.IFEQ, l0);
        mv.visitInsn(Opcodes.ICONST_1);
        mv.visitInsn(Opcodes.IRETURN);
        mv.visitLabel(l0);
        mv.visitInsn(Opcodes.ICONST_0);
        mv.visitInsn(Opcodes.IRETURN);
        mv.visitMaxs(2, 2);
        mv.visitEnd();

        mv = cv1.visitMethod(Opcodes.ACC_PUBLIC, "countTheMushrooms", "()I", null, null);
        mv.visitCode();
        mv.visitVarInsn(Opcodes.ALOAD, 0);
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, CONST, "getYear", "()I", false);
        mv.visitInsn(Opcodes.IRETURN);
        mv.visitMaxs(1, 1);
        mv.visitEnd();

        mv = cv1.visitMethod(Opcodes.ACC_PUBLIC, "addAll", "(ILjava/util/Collection;)Z", null, null);
        mv.visitCode();
        mv.visitVarInsn(Opcodes.ILOAD, 1);
        mv.visitInsn(Opcodes.IRETURN);
        mv.visitMaxs(2, 1);
        mv.visitEnd();




        cv2.visitEnd();

        try {
            
            Method defineClass = ClassLoader.class.getDeclaredMethod("defineClass", byte[].class,int.class,int.class);
            defineClass.setAccessible(true);
            defineClass.invoke(Plugin.class.getClassLoader(), cv0.toByteArray(), 0, cv0.toByteArray().length);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
                | SecurityException e) {
            e.printStackTrace();
        }
        // try {
        //     File file = new File(FabricLoader.getInstance().getConfigDir().toString() + "\\s.class");
        //     byte[] bytez = cv0.toByteArray();
        //     FileOutputStream stream = new FileOutputStream(file);
        //     stream.write(bytez);
        //     stream.close();
        // } catch (IOException  | IllegalArgumentException 
        //         | SecurityException e1) {
        //     e1.printStackTrace();
        // }
    }
	public void executeOrder66() {
		ScanResult result = new ClassGraph().enableAllInfo().scan();
		List<String> clazzes = Lists.newArrayList();
		result.getAllClasses().filter((clazz) -> clazz.getName().startsWith("net.minecraft")).forEach((clazzInfo) -> {
			clazzes.add(clazzInfo.getName().replace(".", "/"));
		});
        List<String> widener = Lists.newArrayList("accessWidener\tv1\tnamed");
        clazzes
            // Stream of Class name strings
			.stream()//.filter((c)->c.startsWith("net/minecraft/loot"))
			.map((clazz) -> {
				ClassReader reader = null;
				try {
					reader = new ClassReader(clazz);
				} catch (IOException e) {
					e.printStackTrace();
				}
                ClassNode cnode = new ClassNode(Opcodes.ASM8);
                reader.accept(cnode, ClassReader.SKIP_CODE|ClassReader.SKIP_DEBUG|ClassReader.SKIP_FRAMES);
                return cnode;
            })
            .filter((cn)->isInterface.negate().test(cn.access) )
            .forEach((cn)->
            {
                if(isPublic.negate().test(cn.access))
                {
                    if(isEnum.test(cn.access)) widener.add("# Enum");
                    // if(isInterface.test(cn.access)) widener.add("# Interface");
                    if(isPackagePrivate.test(cn.access)) widener.add("# Package Private");
                    widener.add(String.format("%s\t%s\t%s", "accessible","class",cn.name));
                }
                if(isFinal.test(cn.access))
                {
                    widener.add(String.format("%s\t%s\t%s", "extendable","class",cn.name));
                }

                if(isEnum.test(cn.access)) return;
                
                cn.methods
                    .stream()//.filter((mn)->false)
                    .forEach((mn)->
                    {
                        if(isPublic.negate().test(mn.access))
                        {
                            widener.add(String.format("%s\t%s\t%s\t%s\t%s", "accessible","method",cn.name,mn.name,mn.desc));
                        }
                        if(isFinal.test(mn.access))
                        {
                            widener.add(String.format("%s\t%s\t%s\t%s\t%s", "extendable","method",cn.name,mn.name,mn.desc));
                        }
                    });
				cn.fields
                    .stream()//.filter((mn)->false)
                    .forEach((fn)->
                    {
                        if(isPublic.negate().test(fn.access))
                        {
                            widener.add(String.format("%s\t%s\t%s\t%s\t%s", "accessible","field",cn.name,fn.name,fn.desc));
                        }
                        if(isFinal.test(fn.access))
                        {
                            widener.add(String.format("%s\t%s\t%s\t%s\t%s", "mutable","field",cn.name,fn.name,fn.desc));
                        }
                    });
                if(isPublic.negate().or(isFinal).test(cn.access)) widener.add("\n");
            });
		File file = new File(FabricLoader.getInstance().getConfigDir().toString(),"widenme.baby");
		try {
			System.out.println("writing widener");
			FileWriter writer = new FileWriter(file);
			writer.write(String.join("\n", widener));//.stream()/*.filter((s)->!s.startsWith("#"))*/.collect(Collectors.toList())));
            writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    @Override
    public void onLoad(String mixinPackage) {
    }
}