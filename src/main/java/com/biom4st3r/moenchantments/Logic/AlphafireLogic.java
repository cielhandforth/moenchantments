package com.biom4st3r.moenchantments.logic;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.biom4st3r.moenchantments.MoEnchantsConfig;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.interfaces.PlayerExperienceStore;
import com.google.common.collect.Sets;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.function.ApplyBonusLootFunction.OreDrops;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.RecipeType;
import net.minecraft.recipe.SmeltingRecipe;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;

/**
 * AlphafireLogic
 */
public class AlphafireLogic {

    // public static Tag<Block> blacklist = TagRegistry.block(new Identifier(ModInit.MODID, "alphafire_blacklist"));
    static Set<Item> items_without_fortune = Sets.newHashSet();

    private static OreDrops formula = new OreDrops();

    public static void attemptSmeltStacks(ServerWorld world,Entity entity, ItemStack tool, List<ItemStack> dropList)
    {
        
        if(items_without_fortune.isEmpty() && MoEnchantsConfig.config.ApplyLootingToAlphaFire)
        {
            items_without_fortune = ModInit.blocks_without_loot_function.stream().map((b)->b.asItem()).peek((i)->ModInit.logger.debug(i.toString())).collect(Collectors.toSet());
            ModInit.blocks_without_loot_function = null;
        }
        RecipeManager rm = world.getRecipeManager(); 
        Inventory basicInv = new SimpleInventory(1);
        
        ItemStack itemToBeChecked = ItemStack.EMPTY;
        Optional<SmeltingRecipe> smeltingResult;
        
        for(int stacksIndex = 0; stacksIndex < dropList.size(); stacksIndex++)
        {
            itemToBeChecked = dropList.get(stacksIndex);
            basicInv.setStack(0, itemToBeChecked);
            smeltingResult = rm.getFirstMatch(RecipeType.SMELTING, basicInv, entity.world);
            if(smeltingResult.isPresent() && !(ModInit.autoSmelt_blacklist.contains(itemToBeChecked.getItem())))
            {
                int count = itemToBeChecked.getCount();
                if(items_without_fortune.contains(itemToBeChecked.getItem()) && EnchantmentHelper.getLevel(Enchantments.FORTUNE, tool) > 0)
                {
                    // count = loot_functon.formula.getValue(world.getRandom(), count, EnchantmentHelper.getLevel(Enchantments.FORTUNE, tool));
                    count = formula.getValue(world.getRandom(), count, EnchantmentHelper.getLevel(Enchantments.FORTUNE, tool));
                    if(MoEnchantsConfig.config.AlphaFireCauseExtraDamage &&  entity instanceof PlayerEntity)
                    {
                        tool.damage(Math.max(count-1, 3), (PlayerEntity)entity,(playerEntity_1) -> {
                            playerEntity_1.sendToolBreakStatus(((PlayerEntity)entity).getActiveHand());
                        });
                    }
                }
                dropList.set(stacksIndex, new ItemStack(smeltingResult.get().getOutput().getItem(),count));
                ((PlayerExperienceStore)(Object)entity).addExp(smeltingResult.get().getExperience() * itemToBeChecked.getCount());
                if(!ModInit.lockAutoSmeltSound)
                {
                    ModInit.lockAutoSmeltSound = true;
                    world.playSound((PlayerEntity)null, entity.getBlockPos(), SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.1f, 0.1f);
                }
            }
        }
    }

}