package com.biom4st3r.moenchantments.logic;

import java.util.ArrayList;
import java.util.Queue;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.MoEnchantsConfig;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.interfaces.PlayerExperienceStore;
import com.google.common.collect.Lists;
import com.google.common.collect.Queues;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CropBlock;
import net.minecraft.block.Material;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.tag.BlockTags;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

public final class VeinMinerLogic
{

	// public static Tag<Block> validBlocks = TagRegistry.block(new Identifier(ModInit.MODID, "veinmine"));

	public static void tryVeinMining(World world,BlockPos blockPos,BlockState blockState,PlayerEntity pe)
    {
		// ModInit.logger.debug("validBlocks size: %s", validBlocks.values().size());
		if(world.isClient) return;
		if(pe==null) return; // Patch for Adorn CarpetedBlock.
		
		ItemStack tool = pe.getMainHandStack();
		if(tool == ItemStack.EMPTY || !tool.hasEnchantments())
		{
			return;
		}
		int VeinMinerLvl = EnchantmentRegistry.VEINMINER.getLevel(tool);
		int TreeFellerLvl = EnchantmentRegistry.TREEFELLER.getLevel(tool);
		boolean hasAutoSmelt = EnchantmentRegistry.AUTOSMELT.hasEnchantment(tool);

		Block currentType = blockState.getBlock();
		int brokenBlocks = 0;

		if(TreeFellerLvl > 0)
        {
            if(isValidBlockForAxe( blockState))
            {
				brokenBlocks = doVeinMiner(blockState,world,blockPos,MoEnchantsConfig.config.TreeFellerMaxBreakByLvl[TreeFellerLvl-1],pe,tool);
				ModInit.logger.debug("%s block broken",brokenBlocks);
			}
		}
		else if(VeinMinerLvl > 0)
		{
			if(isValidBlockForPick(blockState))
			{
				brokenBlocks = doVeinMiner(blockState,world,blockPos,MoEnchantsConfig.config.VeinMinerMaxBreakByLvl[VeinMinerLvl-1],pe,tool);
				ModInit.logger.debug("%s block broken",brokenBlocks);
			}
		}
		pe.increaseStat(Stats.MINED.getOrCreateStat(currentType), brokenBlocks);
		if(hasAutoSmelt)
		{
			int retrievedXp = ((PlayerExperienceStore)(Object)pe).biom4st3r_getAndRemoveStoredExp();
			ModInit.logger.debug("retrieved XP %s", retrievedXp);
			for(; retrievedXp > 0; retrievedXp--)
			{
				dropExperience(1, blockPos, world);
			}
		}
	}

	public static ArrayList<CropBlock> getCropBlocks(World w, BlockPos source, Block type)
	{
		return null;
	}

	private static boolean isValidBlockForPick(BlockState state)
	{
		if(state.getBlock() == Blocks.STONE || state.getBlock() == Blocks.ANDESITE || state.getBlock() ==Blocks.GRANITE || state.getBlock() == Blocks.DIORITE || state.isAir())
		{
			return false;
		}
		else if(ModInit.int_block_whitelist.contains(Registry.BLOCK.getRawId(state.getBlock())))
		{
			return true;
		}
		ModInit.logger.debug("%s global false",state.toString());
		return false;
		
	}

	private static boolean isValidBlockForAxe(BlockState state)
	{
		// String path = Registry.BLOCK.getId(state.getBlock()).getPath();
		if(state.getMaterial() == Material.WOOD) return true;
		if(BlockTags.LOGS.contains(state.getBlock())) return true;
		return false;
	}
	
	private static void dropExperience(float experienceValue ,BlockPos blockPos,World world)
	{
		world.spawnEntity(new ExperienceOrbEntity(world, blockPos.getX(), blockPos.getY(), blockPos.getZ(), (int)experienceValue));
	}

	public static ArrayList<BlockPos> getSameBlocks(World w,BlockPos source, Block type)
    {
		ArrayList<BlockPos> t = Lists.newArrayList();
		BlockPos[] poses = new BlockPos[] {
			source.down().north(),source.down().east(),source.down().west(),source.down().south(),
			source.up(),source.down(),source.north(),source.east(),source.south(),source.west(),
			source.up().south(),source.up().east(),source.up().west(),source.up().north()};
		/*
		poses = new BlockPos[] {source.up(),source.down(),source.north(),source.east(),source.south(),source.west(),
			source.up().south(),source.up().east(),source.up().west(),source.up().north(),
			source.down().north(),source.down().east(),source.down().west(),source.down().south()};
		*/

		for(BlockPos pos : poses)
        {
            if(w.getBlockState(pos).getBlock() == type)
            {
                t.add(pos);
            }
		}
		
        return t;
	}

	private static int doVeinMiner(BlockState blockState,World world,BlockPos blockPos,int maxBlocks,PlayerEntity pe,ItemStack tool)
	{
		ModInit.logger.debug("BlockType %s", blockState.getBlock());
		// int blocksBroken = 0;
		LongOpenHashSet used = new LongOpenHashSet(29);
		Queue<BlockPos> toBreak = Util.make(Queues.newArrayBlockingQueue(29), (queue)->queue.add(blockPos));

		while(!toBreak.isEmpty() && used.size() <= (maxBlocks))
		{
			BlockPos currPos = toBreak.poll();
			BlockState currentState = world.getBlockState(currPos);
			used.add(currPos.asLong());
			if(toBreak.size() < 50)
			{
				for (BlockPos blockPos2 : getSameBlocks(world, currPos, blockState.getBlock())) 
				{
					if(world.getBlockState(blockPos2).getBlock() == currentState.getBlock() && !used.contains(blockPos2.asLong()))
					{
						toBreak.offer(blockPos2);
					}
				}
			}

			Block.dropStacks(currentState, world, pe.getBlockPos(), null, pe, tool);
			world.breakBlock(currPos, false);
			currentState.getBlock().onBreak(world, currPos, currentState, pe);
			tool.postMine(world, currentState, currPos, pe);
			
			tool.damage(1, pe,(playerEntity_1) -> {
				playerEntity_1.sendToolBreakStatus(pe.getActiveHand());
			});
			if(MoEnchantsConfig.config.ProtectItemFromBreaking ? tool.getMaxDamage()-tool.getDamage() < 5 : false)
			{
				pe.playSound(SoundEvents.ENTITY_ITEM_BREAK, SoundCategory.PLAYERS, 1f, 1f);
				pe.sendMessage(new TranslatableText("dialog.biom4st3rmoenchantments.tool_damage_warning_veinminer").formatted(Formatting.GREEN),false);
				break;
			}
		}
		ModInit.lockAutoSmeltSound = false;
		pe.addExhaustion(0.005F * used.size());
		return used.size();
	}
}