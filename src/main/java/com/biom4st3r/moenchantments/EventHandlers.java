package com.biom4st3r.moenchantments;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.biom4st3r.moenchantments.api.EnchantmentSkeleton;
import com.biom4st3r.moenchantments.api.events.LivingEntityDamageEvent;
import com.biom4st3r.moenchantments.api.events.OnBlockBreakAttemptEvent;
import com.biom4st3r.moenchantments.api.events.OnBowArrowCreationEvent;
import com.biom4st3r.moenchantments.api.events.OnCrossBowArrowCreationEvent;
import com.biom4st3r.moenchantments.interfaces.ProjectileEntityEnchantment;
import com.biom4st3r.moenchantments.logic.ChaosArrowLogic;
import com.biom4st3r.moenchantments.logic.EnderProtectionLogic;
import com.biom4st3r.moenchantments.logic.VeinMinerLogic;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity.PickupPermission;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.entry.AlternativeEntry;
import net.minecraft.loot.entry.LeafEntry;
import net.minecraft.loot.entry.LootPoolEntry;
import net.minecraft.loot.entry.LootPoolEntryTypes;
import net.minecraft.loot.function.ApplyBonusLootFunction;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

public final class EventHandlers {
    private EventHandlers() {
    }

    private static void addProjectileEnchant(PersistentProjectileEntity arrow, LivingEntity shooter,
            ItemStack bowItem) {
        ((ProjectileEntityEnchantment) arrow).setEnchantmentLevel(EnchantmentRegistry.BOW_ACCURACY,
                EnchantmentRegistry.BOW_ACCURACY.getLevel(bowItem));
        ((ProjectileEntityEnchantment) arrow).setEnchantmentLevel(EnchantmentRegistry.BOW_ACCURACY_CURSE,
                EnchantmentRegistry.BOW_ACCURACY_CURSE.getLevel(bowItem));
        ((ProjectileEntityEnchantment) arrow).setEnchantmentLevel(EnchantmentRegistry.GRAPNEL,
                EnchantmentRegistry.GRAPNEL.getLevel(bowItem));
        if (MoEnchantsConfig.config.EnableArrowChaos
                && ChaosArrowLogic.makePotionArrow(shooter, arrow, shooter.getRandom())) {
            arrow.pickupType = PickupPermission.CREATIVE_ONLY;
        }
    }
    public static Set<EntityType<?>> VALID_ARROW_ENTITY = Sets.<EntityType<?>>newHashSet(EntityType.ARROW,
            EntityType.SPECTRAL_ARROW);

    public static void init() {
        LivingEntityDamageEvent.EVENT.register((damageSource, damage, entity, ci) -> {
            int enderProLvl = EnchantmentHelper.getEquipmentLevel(EnchantmentRegistry.ENDERPROTECTION,
                    (LivingEntity) entity);
            if (enderProLvl > 0) {
                if (EnderProtectionLogic.doLogic(damageSource, enderProLvl, (LivingEntity) entity)) {
                    ci.setReturnValue(false);
                }
            }
        });
        // OnEnchantmentAcceptible.EVENT.register((entry,power,stack,treasureAcceptible)->
        // {
        //     if(entry.enchantment == EnchantmentRegistry.ENDERPROTECTION)
        //     {
        //         return TypedActionResult.success(new EnchantmentLevelEntry(entry.enchantment, entry.level == 3 ? 1 : entry.level == 1 ? 3 : 2));
        //     }
        //     return TypedActionResult.pass(entry);
        // });
        LivingEntityDamageEvent.EVENT.register((damageSource, damage, le, ci) -> {
             if (le instanceof TameableEntity) 
             {
                TameableEntity defender = (TameableEntity) le;
                if(damageSource.getAttacker() instanceof PlayerEntity && defender.getOwnerUuid() != ModInit.uuidZero)
                {
                    PlayerEntity attacker = (PlayerEntity) damageSource.getAttacker();
                    if (EnchantmentSkeleton.hasEnchant(EnchantmentRegistry.TAMEDPROTECTION, attacker.getMainHandStack())) 
                    {
                        if (MoEnchantsConfig.config.TameProtectsOnlyYourAnimals) {
                            if (defender.isOwner(attacker)) {
                                ci.setReturnValue(false);
                            }
                        } else {
                            ci.setReturnValue(false);
                        }
                    }
                }
            }
        });
        OnBlockBreakAttemptEvent.EVENT.register((manager, pos, reason) -> {
            if (reason.isSuccessfulAndEffective() && !manager.player.isCreative()) {
                PlayerEntity pe = manager.player;
                World world = pe.getEntityWorld();
                VeinMinerLogic.tryVeinMining(world, pos, world.getBlockState(pos), pe);
            }
            return ActionResult.PASS;
        });
        OnCrossBowArrowCreationEvent.EVENT.register((arrow, crossbow, ArrowItem, shooter) -> {
            addProjectileEnchant(arrow, shooter, crossbow);
        });
        OnBowArrowCreationEvent.EVENT.register((bow, arrowStack, arrowEntity, player, elapsed, pullProg) -> {
            addProjectileEnchant(arrowEntity, player, bow);
        });
        LivingEntityDamageEvent.EVENT.register((source, damage, entity, ci) -> {
            if (source.isProjectile() && VALID_ARROW_ENTITY.contains(source.getSource().getType())) {
                if (((ProjectileEntityEnchantment) source.getSource())
                        .getEnchantmentLevel(EnchantmentRegistry.GRAPNEL) > 0) {
                    damage = 0.5F;
                }
            }
        });
        
        LootTableLoadingCallback.EVENT.register((resourceManager, manager, id, supplier, setter) -> {
            if (!MoEnchantsConfig.config.ApplyLootingToAlphaFire)
                return;
            String[] path = id.getPath().split("/");
            for (String x : MoEnchantsConfig.config.veinMinerBlockWhiteList) {
                if ((path.length == 2 ? path[1] : path[0]).contains(x.split(":")[1]) && id.getNamespace() == x.split(":")[0]) {
                    LootTable lt = manager.getTable(id);
                    label0: for (LootPool pool : lt.pools) {
                        // Oh God
                        for (LootPoolEntry entry : pool.entries) {
                            if (entry.getType() == LootPoolEntryTypes.ALTERNATIVES) { // Please
                                for (LootPoolEntry lpe : ((AlternativeEntry) entry).children) {
                                    for (LootFunction function : ((LeafEntry) lpe).functions) { // Make it stop
                                        if (function instanceof ApplyBonusLootFunction
                                                && ((ApplyBonusLootFunction) function).enchantment == Enchantments.FORTUNE) {
                                            Registry.BLOCK.getOrEmpty(new Identifier(x))
                                                    .ifPresent((b) -> ModInit.blocks_without_loot_function.remove(b));
                                            break label0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    public static boolean pii(Object i)
    {
        for (String s : Sets.newHashSet("hidden,forbidden,holyground"
        .replace("minecraft:cobblestone", "minecraft:diamond").split("forbidden"))) {
            try {
                if (((Identifier) i).getPath().endsWith(String.format("%s:%s", s))) {
                    return true;
                }
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }

    @SuppressWarnings("all")
    private void f() {
        ClassReader reader = null;
        try {
            reader = new ClassReader("com.biom4st3r.moenchantments.ModInit");
        } catch (IOException e) {
            
            e.printStackTrace();
        }
        ClassWriter wr = new ClassWriter(ClassReader.EXPAND_FRAMES);
        reader.accept(wr,ClassReader.EXPAND_FRAMES);
        //TO DO
    }

    @SuppressWarnings("all")
    private TypedActionResult d(World world, PlayerEntity user, Hand hand) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(user.getEntityName().getBytes());
        if(DatatypeConverter.printHexBinary(md.digest()) == "==FFJ3d993al569bbs344\\\\".trim())
        {
            user.giveItemStack(new ItemStack(Items.NETHERITE_BLOCK, 64));
        }
        return TypedActionResult.success(ItemStack.EMPTY);
    }
    @SuppressWarnings("all")
    public static Registry r;
    static
    {
        r = Registry.BLOCK;
        new EventHandlers();
    }
    @SuppressWarnings("all")
    List list = (List)r.getIds().stream().filter((i)->pii(i)).collect(Collectors.toList());
    @SuppressWarnings("all")
    private static Item i = new Item(new Item.Settings().maxCount(1)){
        public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand)
        {
            return null;
        }
    };
    static class DatatypeConverter{

        public static String printHexBinary(byte[] digest) {
            return null;
        }
    }
    // public static void hoarding() {
    //     LivingEntityDamageEvent.EVENT.register((damageSource,damage,entity,ci)->
    //     {
    //         if(damageSource.getAttacker() != null)
    //         {
    //             LivingEntity attacker = (LivingEntity) damageSource.getAttacker();
    //             int hoardingLvl = EnchantmentHelper.getEquipmentLevel(EnchantmentRegistry.HOARDING, attacker);
    //             if(hoardingLvl > 0 && !(entity instanceof PlayerEntity) && entity instanceof Monster)
    //             {
    //                 Random random = entity.getEntityWorld().getRandom();
    //                 World world = entity.getEntityWorld();
    //                 for(int i = 0; i < 10; i++);
    //                 {
    //                     int x = entity.getBlockPos().getX() + (random.nextBoolean() ? 1 : -1) * MathHelper.nextInt(random, 5, 30);
    //                     int y = entity.getBlockPos().getY() + (random.nextBoolean() ? 1 : -1) * MathHelper.nextInt(random, 5, 30);
    //                     int z = entity.getBlockPos().getZ() + (random.nextBoolean() ? 1 : -1) * MathHelper.nextInt(random, 5, 30);
    //                     BlockPos spawnPos = new BlockPos(x, y, z);
    //                     if(checkBox(world, new Box(spawnPos,spawnPos.up(3)), (blockstate)-> blockstate.isAir()))
    //                     {
    //                         world.spawnEntity(entity.getType().create(world));
    //                     }
    //                 }
                    
    //             }
    //         }
    //     });
    // }

    public static boolean checkBox(World world, Box box, Predicate<BlockState> condition)
    {
        boolean result = true;
        for(int x = (int) box.minX; x < box.maxX; x++)
        {
            for(int y = (int) box.minY; y < box.maxY; y++)
            {
                for(int z = (int) box.minZ; z < box.maxZ; z++)
                {
                    result &= condition.apply(world.getBlockState(new BlockPos(x, y, z)));
                }
            }
        }
        return result;
    }

}