package com.biom4st3r.moenchantments;

import java.util.function.Function;

import com.biom4st3r.moenchantments.api.EnchantmentSkeleton;
import com.biom4st3r.moenchantments.api.MoEnchantBuilder;
import com.google.common.collect.Sets;

import net.minecraft.enchantment.Enchantment.Rarity;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.AxeItem;
import net.minecraft.item.BowItem;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.FishingRodItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MiningToolItem;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.SwordItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

/**
 * EnchantmentRegistry
 * Centeral store for Enchantment instances and registry
 */
public class EnchantmentRegistry {
    public static final String MODID = ModInit.MODID;
    public static final EquipmentSlot[] 
        ARMOR_SLOTS = new EquipmentSlot[] {EquipmentSlot.CHEST,EquipmentSlot.FEET,EquipmentSlot.HEAD,EquipmentSlot.LEGS},
        ALL_SLOTS = new EquipmentSlot[] {EquipmentSlot.CHEST,EquipmentSlot.FEET,EquipmentSlot.HEAD,EquipmentSlot.LEGS,EquipmentSlot.MAINHAND,EquipmentSlot.OFFHAND},
        HAND_SLOTS = new EquipmentSlot[] {EquipmentSlot.MAINHAND,EquipmentSlot.OFFHAND};
    
    private static Function<ItemStack,Boolean> isBowOrCrossBow = (stack)->
    {
        for(Class<?> clazz : Sets.<Class<?>>newHashSet(FishingRodItem.class,BowItem.class,CrossbowItem.class))
        {
            if(clazz.isInstance(stack.getItem())) return true;
        }
        return false;
    };

    // 1 5 - 35
    // 2 10 - 40
    // 3 15 - 45
    public static final EnchantmentSkeleton TREEFELLER = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        // EnchantmentHelper
        .maxlevel(MoEnchantsConfig.config.TreeFellerMaxBreakByLvl.length)
        .isAcceptible((itemstack)->{return itemstack.getItem() instanceof AxeItem;})
        .minpower((level)->{return level*5;})
        .enabled(MoEnchantsConfig.config.EnableTreeFeller)
        .build("treefeller");
    // 1 5 - 35
    // 2 10 - 40
    // 3 15 - 45
    public static final EnchantmentSkeleton VEINMINER = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .maxlevel(MoEnchantsConfig.config.VeinMinerMaxBreakByLvl.length)
        .isAcceptible((itemStack)->{return itemStack.getItem() instanceof PickaxeItem;})
        .minpower((level)->{return level*5;})
        .maxpower((level)->level*10)
        .enabled(MoEnchantsConfig.config.EnableVeinMiner)
        .build("veinminer");
    // 1 30 - 50
    public static final EnchantmentSkeleton AUTOSMELT = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .addExclusive(Enchantments.SILK_TOUCH)
        .enabled(MoEnchantsConfig.config.EnableAutoSmelt)
        .isAcceptible((itemstack)->{return itemstack.getItem() instanceof MiningToolItem;})
        .addExclusive(Enchantments.SILK_TOUCH)
        .minpower((level)->30)
        .maxpower((leve)->50)
        .build("autosmelt");
    // 1 5 - 35
    static boolean instanceOfAny(Object o, Class<?>... clazzes)
    {
        for(Class<?> clazz : clazzes)
        {
            if(clazz.isInstance(o))
            {
                return true;
            }
        }
        return false;
    }
    public static final EnchantmentSkeleton TAMEDPROTECTION = new MoEnchantBuilder(Rarity.COMMON, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .isAcceptible((itemStack)->instanceOfAny(itemStack.getItem(), SwordItem.class,AxeItem.class,BowItem.class,CrossbowItem.class))
        .treasure(true)
        .minpower((level)->5)
        .enabled(MoEnchantsConfig.config.EnableTamedProtection)
        .build("tamedprotection");
    // 1 1
    // 2 20 - 50
    // 3 30 - 60
    public static final EnchantmentSkeleton ENDERPROTECTION = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.ARMOR, ARMOR_SLOTS)
        .maxlevel(3)
        .minpower(level->(level)*10)
        .maxpower(level-> (level)*11)
        .isAcceptibleInAnvil((is,as)-> false)
        .treasure(true)
        .curse(true)
        .enabled(MoEnchantsConfig.config.EnableEnderProtection)
        .build("curseofender");
    // public static final EnchantmentSkeleton HAVESTER = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
    //     .minpower((level)->{return level*4;})
    //     .isAcceptible((itemStack)->{return itemStack.getItem() instanceof HoeItem;})
    //     .enabled(false)
    //     .build("harvester");
    //Gitlab @Mr Cloud
    // 1 21 - 51
    public static final EnchantmentSkeleton SOULBOUND = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.WEARABLE, ALL_SLOTS)
        .enabled(MoEnchantsConfig.config.EnableSoulBound)
        .maxlevel(MoEnchantsConfig.config.UseStandardSoulboundMechanics ? 1 : MoEnchantsConfig.config.MaxSoulBoundLevel)
        .minpower((level)->{return 20+level;})
        .isAcceptible((itemstack)->{return true;})
        .build("soulbound");
    public static final EnchantmentSkeleton POTIONRETENTION = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .minpower((level)->{return 7*level;})
        .maxlevel(MoEnchantsConfig.config.PotionRetentionMaxLevel)
        .isAcceptible((itemstack)->{return itemstack.getItem() instanceof SwordItem || itemstack.getItem() instanceof AxeItem;})
        .enabled(MoEnchantsConfig.config.EnablePotionRetention)
        .build("potionretension");
    // someone responding to Jeb_ on reddit.
    // 1 10 - 40
    // 2 20 - 50
    public static final EnchantmentSkeleton BOW_ACCURACY = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.BOW, HAND_SLOTS)
        .maxlevel(2)
        .minpower((level)->{return level*10;})
        .enabled(MoEnchantsConfig.config.EnableAccuracy && !ModInit.extraBowsFound)
        .isAcceptible(isBowOrCrossBow)
        .build("bowaccuracy");
    public static final EnchantmentSkeleton BOW_ACCURACY_CURSE = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.BOW, HAND_SLOTS)
        .maxlevel(1)
        .curse(true)
        .minpower((l)->20)
        .maxpower((l)->60)
        .addExclusive(BOW_ACCURACY)
        .isAcceptible(isBowOrCrossBow)
        .enabled(MoEnchantsConfig.config.EnableInAccuracy && !ModInit.extraBowsFound)
        .build("bowinaccuracy");
    // Gitlab @cryum
    //1 30 - 60
    public static final EnchantmentSkeleton ARROW_CHAOS = new MoEnchantBuilder(Rarity.VERY_RARE,EnchantmentTarget.BOW,HAND_SLOTS)
        .curse(true)
        .isAcceptible(isBowOrCrossBow)
        .minpower((level)->{return 30;})
        .enabled(MoEnchantsConfig.config.EnableArrowChaos && !ModInit.extraBowsFound)
        .build("arrow_chaos");
    // cryum from issue #13
    // 1 16 - 46
    // 2 32 - 62
    public static final EnchantmentSkeleton GRAPNEL = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.BOW, HAND_SLOTS)
        .minpower(level->20)
        .maxlevel(1)
        .treasure(true)
        .enabled(MoEnchantsConfig.config.EnableGrapnel)
        .isAcceptible(isBowOrCrossBow)
        .addExclusive(Enchantments.MULTISHOT).addExclusive(ARROW_CHAOS)
        .build("grapnel");


    public static void init()
    {
        for(EnchantmentSkeleton e : new EnchantmentSkeleton[] {
            TREEFELLER,VEINMINER,AUTOSMELT,
            TAMEDPROTECTION,ENDERPROTECTION,POTIONRETENTION,
            SOULBOUND,BOW_ACCURACY,
            BOW_ACCURACY_CURSE,ARROW_CHAOS,GRAPNEL,
            })
        {
            if(e.enabled())
                Registry.register(Registry.ENCHANTMENT, new Identifier(MODID, e.regName()), e);
        }
    }
}