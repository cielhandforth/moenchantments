package com.biom4st3r.moenchantments.interfaces;

import net.minecraft.entity.effect.StatusEffectInstance;

public interface PotionRetentionTarget
{
    public boolean applyRetainedPotionEffect(StatusEffectInstance statuseffect);
}