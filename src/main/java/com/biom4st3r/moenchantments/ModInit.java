package com.biom4st3r.moenchantments;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.ChestBlockEntity;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.Util;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import com.biom4st3r.moenchantments.networking.Packets;
import com.google.common.collect.Sets;

import org.apache.commons.lang3.tuple.Pair;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.fabricmc.loader.api.FabricLoader;

public class ModInit implements ModInitializer {
	public static final String MODID = "biom4st3rmoenchantments";
	public static final BioLogger logger = new BioLogger("MoEnchantments");
	public static final UUID uuidZero = new UUID(0L, 0L);
	public static IntArrayList int_block_whitelist = new IntArrayList(100);
	public static List<Item> autoSmelt_blacklist = new ArrayList<Item>();
	public static boolean lockAutoSmeltSound = false;
	public static Set<Block> blocks_without_loot_function = Sets.newHashSet();
	public static boolean extraBowsFound = false;

	public static void enchantmentTest()// EnchantmentSkeleton mEnch, ItemStack stack)
	{
		System.out.println("hello there");
	}

	@Override
	public void onInitialize() {
		// VertexBuffer
		new Identifier("giftbox");
		extraBowsFound = FabricLoader.getInstance().isModLoaded("extrabows");
		EnchantmentRegistry.init();
		EventHandlers.init();
		Packets.init();
		if(FabricLoader.getInstance().isDevelopmentEnvironment()) test();
	}

	public static void whitelistToBlock() {
		for (String s : MoEnchantsConfig.config.veinMinerBlockWhiteList) {
			Block block = Registry.BLOCK.get(new Identifier(s));
			if (block != Blocks.AIR) 
			{
				blocks_without_loot_function.add(block);
				int_block_whitelist.add(Registry.BLOCK.getRawId(block));
				logger.log("added %s as %s", block.getLootTableId().toString(),Registry.BLOCK.getRawId(block));
				continue;
			}
			else
			{
				logger.log("%s was not found", s);
			}
		}
	}

	public static void blacklistAutosmelt()
	{
		for(String s : MoEnchantsConfig.config.AutoSmeltBlackList)
		{
			Item i = Registry.ITEM.get(new Identifier(s));
			if(i != Items.AIR)
			{
				autoSmelt_blacklist.add(i);
				logger.log("added %s to autosmelt blacklist",s);
			}
			else
			{
				logger.log("%s was not found", s);
			}
		}
	}

	@SuppressWarnings("all")
	static void test()
	{
		SimpleInventory inventory = Util.make(new SimpleInventory(27), (inv)->
		{
			Set<Pair<Set<Item>,Set<Enchantment>>> items = Sets.newHashSet(
				Pair.of(
					Sets.newHashSet(Items.DIAMOND_PICKAXE), 
					Sets.newHashSet(Enchantments.FORTUNE,EnchantmentRegistry.VEINMINER)),
				Pair.of(
					Sets.newHashSet(Items.DIAMOND_PICKAXE),
					Sets.newHashSet(EnchantmentRegistry.AUTOSMELT,Enchantments.FORTUNE,EnchantmentRegistry.VEINMINER)),
				Pair.of(
					Sets.newHashSet(Items.DIAMOND_AXE),
					Sets.newHashSet(Enchantments.EFFICIENCY,EnchantmentRegistry.TREEFELLER)),
				Pair.of(
					Sets.newHashSet(Items.DIAMOND_AXE),
					Sets.newHashSet(EnchantmentRegistry.AUTOSMELT,Enchantments.EFFICIENCY,EnchantmentRegistry.TREEFELLER)),
				Pair.of(
					Sets.newHashSet(Items.DIAMOND_SWORD),
					Sets.newHashSet(EnchantmentRegistry.TAMEDPROTECTION,EnchantmentRegistry.SOULBOUND,EnchantmentRegistry.POTIONRETENTION)),
				Pair.of(
					Sets.newHashSet(Items.DIAMOND_BOOTS),
					Sets.newHashSet(EnchantmentRegistry.ENDERPROTECTION,EnchantmentRegistry.SOULBOUND)),
				Pair.of(
					Sets.newHashSet(Items.BOW,Items.CROSSBOW),
					Sets.newHashSet(EnchantmentRegistry.BOW_ACCURACY,EnchantmentRegistry.ARROW_CHAOS)),
				Pair.of(
					Sets.newHashSet(Items.BOW,Items.CROSSBOW),
					Sets.newHashSet(EnchantmentRegistry.BOW_ACCURACY_CURSE,EnchantmentRegistry.GRAPNEL))
				
			);
			items.forEach((pair)->
			{
				for(Item item : pair.getLeft())
				{
					ItemStack is = new ItemStack(item);
					for(Enchantment enchant : pair.getRight())
					{
						int level = enchant.getMaxLevel();
						if(enchant == EnchantmentRegistry.ENDERPROTECTION) level = 1;
						if(enchant == EnchantmentRegistry.SOULBOUND) level = 2;
						is.addEnchantment(enchant, level);
					}
					inv.addStack(is.copy());
				}
			});
			inv.addStack(new ItemStack(Items.ARROW, 64));
			inv.addStack(new ItemStack(Items.BONE,  64));
			inv.addStack(new ItemStack(Items.WOLF_SPAWN_EGG,32));
		});

		CommandRegistrationCallback.EVENT.register((dispatcher, dedicated)->
		{
			dispatcher.register(CommandManager.literal("biom4st3r_test").executes((serverCommandSourceCommandContext)->
			{
				ServerPlayerEntity spe = serverCommandSourceCommandContext.getSource().getPlayer();
				World w = spe.getEntityWorld();
				w.setBlockState(spe.getBlockPos(), Blocks.CHEST.getDefaultState());
				BlockEntity be = w.getBlockEntity(spe.getBlockPos());
				if(be.getType() == BlockEntityType.CHEST)
				{
					ChestBlockEntity cbe = (ChestBlockEntity) be;
					for(int i = 0; i < inventory.size(); i++)
					{
						cbe.setStack(i, inventory.getStack(i));
					}
				}
				return 0;
			}));
		});
		
	}

}